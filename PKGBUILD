# Maintainer: Future Linux Team <future_linux@163.com>

pkgname=cyrus-sasl
pkgver=2.1.28
pkgrel=1
pkgdesc="Cyrus Simple Authentication Service Layer (SASL) library"
arch=('x86_64')
url="https://www.cyrusimap.org/sasl/"
license=('custom')
depends=('glibc' 'gdbm' 'openssl')
makedepends=('krb5' 'libldap' 'sqlite')
options=('!makeflags')
install=${pkgname}.install
source=(https://github.com/cyrusimap/cyrus-sasl/releases/download/${pkgname}-${pkgver}/${pkgname}-${pkgver}.tar.gz
    saslauthd
    saslauthd.conf
    saslauthd.service)
sha256sums=(7ccfc6abd01ed67c1a0924b353e526f1b766b21f42d4562ee635a8ebfc5bb38c
    76d3dc6548dc29a80538ae660ce1c0f3e79016fe54bca8f19adcdfbde8a950ab
    3349d9588ac1553948404c38e2f740b0ad85a6c64850466b5569d7f295c14e0b
    a2f621dbbbe2e67bc1a11cd0363772e48ebb9e0c6a1d720fe793f173c5c90805)

build() {
    cd ${pkgname}-${pkgver}

    ${configure}                             \
        --disable-krb4                       \
        --disable-macos-framework            \
        --disable-otp                        \
        --disable-passdss                    \
        --disable-srp                        \
        --disable-srp-setpass                \
        --disable-static                     \
        --enable-alwaystrue                  \
        --enable-anon                        \
        --enable-auth-sasldb                 \
        --enable-checkapop                   \
        --enable-cram                        \
        --enable-digest                      \
        --enable-gssapi                      \
        --enable-ldapdb                      \
        --enable-login                       \
        --enable-ntlm                        \
        --enable-plain                       \
        --enable-shared                      \
        --sysconfdir=/etc                    \
        --with-dblib=gdbm                    \
        --with-devrandom=/dev/urandom        \
        --with-configdir=/etc/sasl2:/etc/sasl:/usr/lib64/sasl2 \
        --with-sphinx-build=no               \
        --with-ldap                          \
        --with-pam                           \
        --with-saslauthd=/var/run/saslauthd  \
        --with-sqlite3=/usr/lib64

    sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
    make
}

package() {
    cd ${pkgname}-${pkgver}

    make DESTDIR=${pkgdir} install

    # remove files provided by extra/cyrus-sasl
    rm -fv ${pkgdir}/usr/lib64/sasl/lib{gs2,gssapiv2,ldapdb,sql}.so*

    install -v -dm700 ${pkgdir}/var/lib/sasl

    install -Dm644 ${srcdir}/saslauthd         ${pkgdir}/etc/default/saslauthd
    install -Dm644 ${srcdir}/saslauthd.conf    ${pkgdir}/usr/lib/tmpfiles.d/saslauthd.conf
    install -Dm644 ${srcdir}/saslauthd.service ${pkgdir}/usr/lib/systemd/system/saslauthd.service
}
